import { Component, Directive, OnInit } from '@angular/core';
import { AppService } from '../app.services';
import { InicializarComponent } from '../Metodos/inicializar.component'
import { CalendarComponent } from './calendar.component'

declare var $: any;
declare var FastClick: any;

@Component({
  selector: 'agenda-root',
  templateUrl: './agenda.component.html',
})

export class AgendaComponent implements OnInit{
  public data: any;

  constructor(private AppService: AppService){
  }
  
  //Initialize
   ngOnInit() {

        try {
          InicializarComponent.prototype.Inicializar();
        } catch (error) {
          console.log(error);
        }
        //Initialize calendar
        try {
          CalendarComponent.prototype.render();
        } catch (error) {
          console.log(error);
        }
  }
}