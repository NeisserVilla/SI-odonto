import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/toPromise';

@Injectable()


export class CargoService {
    
    private headers = new Headers({'Content-Type': 'application/json'});
    private Url = 'http://localhost:3000/';  // URL to web api
 
    constructor(private http: Http) { }  

    agregar_cargo(data): Promise<any>{
        return this.http
            .post(this.Url + 'api/agregar_cargos', data)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
        
    }
    buscar_cargo(){
        return this.http
            .get(this.Url + 'api/buscar_cargos')
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    eliminar_cargo(data){
        return this.http
            .post(this.Url + 'api/eliminar_cargos', data)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    restaurar_cargo(data){
        return this.http
            .post(this.Url + 'api/restaurar_cargos', data)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    actualizar_cargo(data){
        return this.http
            .post(this.Url + 'api/actualizar_cargos', data)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }

}