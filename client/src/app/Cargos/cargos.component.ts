import { Component, Directive, OnInit } from '@angular/core';
import { CargoService } from './cargo.service'
//Modelo Form
import { CargoClass } from './cargo.class'
import { InicializarComponent } from '../Metodos/inicializar.component'

declare var $: any;
declare var FastClick: any;

@Component({
  selector: 'cargos-root',
  templateUrl: './cargos.component.html',
})

export class CargosComponent implements OnInit{
  public cargos: any;
  public model
  public privilegios = [{id:1, nombre:'Alto privilegio'},{id:3, nombre:'Bajo privilegio'}]
  public estado
  public edit = false
  constructor(private cargoService: CargoService){
     this.model =  new CargoClass('',1,null)
     this.estado = 0;
  }

  //Consulta
  ngOnInit() {
    this.buscarcargo()
    try {
          InicializarComponent.prototype.Inicializar();
        } catch (error) {
          console.log(error);
        }   
  }
  //Nuevo cargo
  nuevocargo(){
    this.cargoService.agregar_cargo(this.model)
    .then(resp => {
      this.cargos = resp.datos;
    });
  }
  //Buscar cargo
  buscarcargo(){
    this.cargoService.buscar_cargo()
    .then(resp => {
      this.cargos = resp.datos;
    });
  }
  //Buscar cargo
  eliminarcargo(cod_car){
    this.cargoService.eliminar_cargo({cod_car:cod_car})
    .then(resp => {
      this.cargos = resp.datos;
    });
  }
  //Restaurar cargo
  restaurarcargo(cod_car){
     this.cargoService.restaurar_cargo({cod_car:cod_car})
    .then(resp => {
      this.cargos = resp.datos;
    });
  }
  editarcargo(data){
    this.model =  new CargoClass(data.nom_car, data.pri_car, data.cod_car)
    this.edit = true
  }
  actualizarcargo(){
    this.cargoService.actualizar_cargo(this.model)
    .then(resp => {
      this.cargos = resp.datos;
      this.limpiar_form()
    });
  }
  limpiar_form(){
    this.edit = false
    this.model =  new CargoClass('',1,null)
  }
  filter(){
    var dat = this.estado
    var arr = this.cargos
  }
}