import { Component, Directive, OnInit } from '@angular/core';
import { AppService } from '../app.services';
import { InicializarComponent } from '../Metodos/inicializar.component'
   
declare var $: any;
declare var FastClick: any;

@Component({
  selector: 'empleados-root',
  templateUrl: './empleados.component.html',
})

export class EmpleadosComponent implements OnInit{
  public data: any;

  constructor(private AppService: AppService){
  }

  //Inicializar
   ngOnInit() {
    try {
          InicializarComponent.prototype.Inicializar();
        } catch (error) {
          console.log(error);
        }  
  }
}