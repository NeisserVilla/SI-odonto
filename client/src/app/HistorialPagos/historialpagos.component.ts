import { Component, Directive, OnInit } from '@angular/core';
import { AppService } from '../app.services';
import { InicializarComponent } from '../Metodos/inicializar.component'
     
declare var $: any;
declare var FastClick: any;

@Component({
  selector: 'historialpagos-root',
  templateUrl: './historialpagos.component.html',
})

export class HistorialPagosComponent implements OnInit{
  public data: any;

  constructor(private AppService: AppService){
  }

  //Inicializar
   ngOnInit() {
    try {
          InicializarComponent.prototype.Inicializar();
        } catch (error) {
          console.log(error);
        }
  }
}