import { Component, Directive, OnInit } from '@angular/core';
import { AppService } from '../app.services';
import { InicializarComponent } from '../Metodos/inicializar.component'
     
@Component({
  selector: 'home-root',
  templateUrl: './home.component.html',
})

export class HomeComponent implements OnInit{
  public data: any;

  constructor(private AppService: AppService){
  }

  //Consulta
   ngOnInit() {
    try {
          InicializarComponent.prototype.Inicializar();
        } catch (error) {
          console.log(error);
        }
  }

  consultabd(){
      this.AppService.getnodebd()
      .then(resp => {
      this.data = resp.datos;
      console.log(this.data);
          
      });
  }
}