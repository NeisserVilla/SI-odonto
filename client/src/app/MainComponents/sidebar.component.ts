import { Component, Directive, OnInit } from '@angular/core';
import { AppService } from '../app.services';

@Component({
  selector: 'sidebar-root',
  templateUrl: './sidebar.component.html',
})

export class SidebarComponent implements OnInit{
  public data: any;

  constructor(private AppService: AppService){
    this.data = [{id:1},{id:2}]
  }

  //Consulta
  ngOnInit() {
    // console.log(this.data.filter(function(e){return (e.id == 2)}))
  }
}