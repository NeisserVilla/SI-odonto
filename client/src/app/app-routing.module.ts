import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

//Main 
import { HomeComponent }     from './MainComponents/home.component';
//Agenda
import { AgendaComponent }   from './Agenda/agenda.component';
//Historial de Pacientes
import { HistorialPacientesComponent }  from './HistorialPacientes/historialpacientes.component';
//Historial de Pagos
import { HistorialPagosComponent }  from './HistorialPagos/historialpagos.component';
//Empleados
import { EmpleadosComponent }  from './Empleados/empleados.component';
//Cargos
import { CargosComponent }  from './Cargos/cargos.component';

const routes: Routes = [
  { path: '', redirectTo: '/Home', pathMatch: 'full' },
  { path: 'Home',  component: HomeComponent },
  { path: 'Agenda',  component: AgendaComponent },
  { path: 'HistorialPacientes',  component: HistorialPacientesComponent },
  { path: 'HistorialPagos',  component: HistorialPagosComponent },
  { path: 'Empleados',  component: EmpleadosComponent },
  { path: 'Cargos',  component: CargosComponent }
];
 
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}