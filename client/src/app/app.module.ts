import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule }    from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { AppService }       from './app.services';

//Service
import { CargoService }       from './Cargos/cargo.service';

//Main components
import { AppComponent }     from './app.component';
import { HomeComponent }    from './MainComponents/home.component';
import { SidebarComponent } from './MainComponents/sidebar.component';
import { NavbarComponent }  from './MainComponents/navbar.component';
import { FooterComponent }  from './MainComponents/footer.component';
import { ControlSidebarComponent } from './MainComponents/controlsidebar.component';
//Agenda
import { AgendaComponent }  from './Agenda/agenda.component';
//Historial de Pacientes
import { HistorialPacientesComponent }  from './HistorialPacientes/historialpacientes.component';
//Historial de Pagos
import { HistorialPagosComponent }  from './HistorialPagos/historialpagos.component';
//Empleados
import { EmpleadosComponent }  from './Empleados/empleados.component';
//Cargos
import { CargosComponent }  from './Cargos/cargos.component';
 
import { MainComponent } from './main.component'; 
@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    NavbarComponent,
    FooterComponent,
    ControlSidebarComponent,
    HomeComponent,
    AgendaComponent,
    HistorialPacientesComponent,
    HistorialPagosComponent,
    EmpleadosComponent,
    CargosComponent,
    MainComponent
  ],  
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [AppService, CargoService ],
  bootstrap: [
    AppComponent,
    SidebarComponent,
    NavbarComponent,
    FooterComponent,
    ControlSidebarComponent
    ]
})
export class AppModule { }
