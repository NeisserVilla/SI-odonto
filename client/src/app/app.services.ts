import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';

import 'rxjs/add/operator/toPromise';

@Injectable()


export class AppService {
    
    private headers = new Headers({'Content-Type': 'application/json'});
    private Url = 'http://localhost:3000/';  // URL to web api
 
    constructor(private http: Http) { }  

    getnodebd(): Promise<any>{
        return this.http
            .get(this.Url + 'api/servicio')
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
        
  }
    
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }

}