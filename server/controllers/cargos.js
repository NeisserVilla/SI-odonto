var pool = require('../core/conexion.js');

function agregar_cargos(req, res) {
    var query = "select * from sp_cargos('AGREGAR','"+req.body.nombre+"',"+req.body.privilegio+", null)";
    pool.query(query).then(function (Tresult) {
      res.json({
            mensaje: 'Consulta almacenado ejecutado correctamente.',
            datos: Tresult.rows,
            filas: Tresult.returnValue
        });
    }).catch(function (err) {
      res.status(500).json({
            mensaje: "No se pudo ejecutar este consulta almacenado",
            error: err
        });
        console.log(err);
    });
}
function buscar_cargos(req, res) {
    var query = "select * from sp_cargos('BUSCAR','',null, null)";
    pool.query(query).then(function (Tresult) {
      res.json({
            mensaje: 'Consulta almacenado ejecutado correctamente.',
            datos: Tresult.rows,
            filas: Tresult.returnValue
        });
    }).catch(function (err) {
      res.status(500).json({
            mensaje: "No se pudo ejecutar este consulta almacenado",
            error: err
        });
        console.log(err);
    });
}
function eliminar_cargos(req, res) {
    var query = "select * from sp_cargos('ELIMINAR','',null, "+req.body.cod_car+")";
    pool.query(query).then(function (Tresult) {
      res.json({
            mensaje: 'Consulta almacenado ejecutado correctamente.',
            datos: Tresult.rows,
            filas: Tresult.returnValue
        });
    }).catch(function (err) {
      res.status(500).json({
            mensaje: "No se pudo ejecutar este consulta almacenado",
            error: err
        });
        console.log(err);
    });
}
function restaurar_cargos(req, res) {
    var query = "select * from sp_cargos('RESTAURAR','',null, "+req.body.cod_car+")";
    pool.query(query).then(function (Tresult) {
      res.json({
            mensaje: 'Consulta almacenado ejecutado correctamente.',
            datos: Tresult.rows,
            filas: Tresult.returnValue
        });
    }).catch(function (err) {
      res.status(500).json({
            mensaje: "No se pudo ejecutar este consulta almacenado",
            error: err
        });
        console.log(err);
    });
}
function actualizar_cargos(req, res) {
    var query = "select * from sp_cargos('ACTUALIZAR','"+req.body.nombre+"',"+req.body.privilegio+", "+req.body.codigo+")";
    pool.query(query).then(function (Tresult) {
      res.json({
            mensaje: 'Consulta almacenado ejecutado correctamente.',
            datos: Tresult.rows,
            filas: Tresult.returnValue
        });
    }).catch(function (err) {
      res.status(500).json({
            mensaje: "No se pudo ejecutar este consulta almacenado",
            error: err
        });
        console.log(err);
    });
}

module.exports = {
    agregar_cargos: agregar_cargos,
    buscar_cargos:buscar_cargos,
    eliminar_cargos:eliminar_cargos,
    restaurar_cargos:restaurar_cargos,
    actualizar_cargos:actualizar_cargos
}