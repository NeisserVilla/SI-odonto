var pool = require('../core/conexion.js');

function servicio(req, res) {
    var query = "SELECT * FROM tb_empleados";
    pool.query(query).then(function (Tresult) {
      res.json({
            mensaje: 'Consulta almacenado ejecutado correctamente.',
            datos: Tresult.rows,
            filas: Tresult.returnValue
        });
    }).catch(function (err) {
      res.status(500).json({
            mensaje: "No se pudo ejecutar este consulta almacenado",
            error: err
        });
        console.log(err);
    });
}

module.exports = {
    servicio: servicio
}