var express = require('express');
var router = express.Router();
var connection = require('../core/conexion').connect;

var services = require("../controllers/services.js");
var cargos = require("../controllers/cargos.js");

// middleware that is specific to this router
router.use(function timeLog(req, res, next) {
  // console.log('Time: ', Date.now());
  next();
});

/* GET users listing. */

connection().then(function () {

  router.get('/', function(req, res, next) {
    res.send('Conexion abierta SI-Odonto');
    console.log('it works!'); 
  });

  router.get('/servicio', function (req, res, next) {
    services.servicio(req, res);
  });

  router.post('/agregar_cargos', function (req, res, next) {
    cargos.agregar_cargos(req, res);
  });

  router.get('/buscar_cargos', function (req, res, next) {
    cargos.buscar_cargos(req, res);
  });
  router.post('/eliminar_cargos', function (req, res, next) {
    cargos.eliminar_cargos(req, res);
  });
  router.post('/restaurar_cargos', function (req, res, next) {
    cargos.restaurar_cargos(req, res);
  });
  router.post('/actualizar_cargos', function (req, res, next) {
    cargos.actualizar_cargos(req, res);
  });
}).catch(function (err) {
  router.post('*', function (req, res) {
    res.send('error en la base de datos');
  });
});

module.exports = router;